<!DOCTYPE html>
<html lang="en">
<head>
    <title>Document</title>
    <link rel="stylesheet" href="error-message.css">
</head>
<body>

        <form id="addform">
        first name :<input type="text" name="fname" id="firstname">
        age: <input type="text" name="lname" id="lastname">
        <input type="submit" id="btn" value="save data">
        </form>
    <table>
        <tr>
            <th id="td1"></th>
        </tr>
    </table>
    <div id="error-message"></div>
    <div id="success-message"></div>

    <script src="jquery.js"></script>
    <script>
        $(document).ready(function()
        {
            
            function loadtable(){
            $.ajax({
                url : "ajax-load.php",
                type : "POST",
                success: function(data){
                    $("#td1").html(data);
                }
            });

        }
        loadtable();
        
        
         $("#btn").on("click",function(e){
             e.preventDefault();
            var fname = $("#firstname").val();
            var lname = $("#lastname").val(); 
            if( fname == "" || lname == ""){
                $("#error-message").html("All fields are required").slideDown();
                $("#success-message").slideUp();
            }else{
                $.ajax({
            url : "insert-data.php",
            type : "POST",
            data : {first_name: fname, last_name: lname},
            success : function(data){
                if(data==1){
                    loadtable();
                    $("#addform").trigger("reset");
                    $("#success-message").html("successfully submitted").slideDown();
                    $("#error-message").slideUp();
                }else{
                    $("#error-message").html("can't save records").slideDown();
                    $("#success-message").slideUp();
                }
                
                }
                });
            }
        });

    });

            


    </script>
    
</body>
</html>