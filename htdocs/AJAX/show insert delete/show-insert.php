<!DOCTYPE html> 
<html lang="en">
<head>
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <form id="erase">
     name :<input type="text" id="fname">
     age :<input type="text" id="age">
    <input type="submit" value="submit"  id="btn">
    </form>
        <table>
            <tr>
                <th id="td1"></th>
            </tr>
        </table>

<div id="error-message"></div>
<div id="success-message"></div>

 <script src="jquery.js"></script>
    <script>
        $(document).ready(function()
        {
            
            function loadtable(){
            $.ajax({
                url : "ajax-load-p.php",
                type : "POST",
                success : function(data){
                    $("#td1").html(data);
                }
            });
        }
        loadtable();

        $("#btn").on("click",function(e){
            e.preventDefault();            
            var firstname = $("#fname").val();
            var fage = $("#age").val();
            if(firstname == "" || fage == ""){
                $("#error-message").html("All field required").slideDown();
                $("#success-message").slideUp();
            }else{

                $.ajax({
                url : "ajax-insert.php",
                type : "POST",
                data : {first_name : firstname , f_age : fage},
                success : function(data){
                    if(data == 1){
                        loadtable();
                        $("#erase").trigger("reset");  
                        $("#success-message").html("Data inserted successfully").slideDown();
                        $("#error-message").slideUp();                      
                    }else{
                        
                        $("#error-message").html("data not inserted").slideDown();
                        $("#success-message").slideUp();
                    }
                }
            });

            }         
        });
    });
    
        
    </script>
    
</body>
</html>