<?php

$conn = getMySqlConnection();

$data = getTableData($conn);
$row = mysqli_num_rows($data);
$date = date("d_M_Y");
$copied_table = "user_details_".$date;
createTable($conn,$row,$copied_table);
insertDataIntoTable($row,$data,$conn,$copied_table);
        


function getMySqlConnection(){
    $conn = mysqli_connect("localhost","root","","copy table");
    return $conn;
}

function getTableData($conn){
    $query = ("SELECT * FROM user_details");            // this table have 1000 dummy records.
    $data = mysqli_query($conn,$query);
    
    return $data;
}

function createTable($conn,$row,$copied_table){
    
    $query1_discription = "DESCRIBE user_details";
    $data1 = mysqli_query($conn,$query1_discription);

    if($data1){
        
        $AI= "auto_increment";
        $query2 = "CREATE TABLE if not exists $copied_table (";
        $y= 1;
        
        while($row1 = mysqli_fetch_array($data1)) {

            if($y==1){
                $field = $row1['Field'];
                $type = $row1['Type'];
                $query2.="$field $type $AI ,";
            }else{
                $field = $row1['Field'];
                $type = $row1['Type'];
                $query2.="$field $type ,";
            }
                $y++;
            // echo "{$row['Field']}    {$row['Type']}"; echo "<br>";
        }
    }else{
        echo "Error in query in line 29"; 
    }
    $query2.=" PRIMARY KEY (user_id));";
    if($row!=0){
        
        $data2 = mysqli_query($conn,$query2);   //table is created here with today's date
        if($data2){
            echo "table created<br>";
        }else{
            echo "table not created ";
        }

    }else{
        echo "No data found";
    }
}

function insertDataIntoTable($row,$data,$conn,$copied_table){

    if($row!=0){
        
        $query3 = "INSERT INTO $copied_table (user_id, username, first_name, last_name, gender, password, status) 
        VALUES ";
        $x=0;
        while($result = mysqli_fetch_assoc($data)){
            
            $user_id = $result['user_id'];
            $username = $result['username'];
            $first_name = $result['first_name'];
            $last_name = $result['last_name'];
            $gender = $result['gender'];
            $password = $result['password'];
            $status = $result['status'];
        
            $query3.= "($user_id, '$username','$first_name','$last_name','$gender','$password','$status')";
            // echo $x."<br>";
                $x++; 
                if($x%100==0){                               // enter data in the batch of 100
                    
                    $data3 = mysqli_query($conn,$query3);
                        // echo "batch of 100 query3 running<br>";
                    
                    $query3 = "INSERT INTO $copied_table ( user_id , username, first_name, last_name, gender, password, status) 
                    VALUES ";
                }else{
                    $query3.=" , ";
                }
        }
        
        if($query3){
            $query3 = rtrim( $query3 , ' , ');              // eleminate the comma from end of the query.
            $data3 = mysqli_query($conn,$query3);
            echo "data inserted into table";
            // echo "trim query3 run";
        }                              
    }else{
    echo "no data found";
    }
}

?>


