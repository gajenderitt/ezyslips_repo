<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Calculator</title>
    <style>
    body{
        background-color:aqua;
        text-align:center;
    }
        form{
            text-align:center;
            font-size:20px;
            margin:auto;
        }
        .button{
            padding:05px 20px;
            margin:20px;
        }
        input{
            margin:10px;
        }
        .output{
            margin:auto;
            font-size:20px;

        }
    </style>
</head>
<body>
<?php
$num1 = $_POST['number1'];
$num2 = $_POST['number2'];
$sum = 0;
if ($_SERVER["REQUEST_METHOD"] == "POST"):
    if (empty($_POST['number1']) || empty($_POST['number2'])):
        $numerr1 = "Enter number please";
    endif;
    if (!preg_match("/[0-9]/", $num1) || !preg_match("/[0-9]/", $num2)):
        $numerr2 = "Please Enter number only";
    endif;
    $sum = sum($num1, $num2);
endif;
function sum($num1, $num2){
    return ($num1 + $num2);
}
?>

    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
    <label for="number1">Enter 1st number :</label>
    <input type="text" name="number1">
    <span style="color: red;">* <?php echo $numerr1; ?>
    <?php echo $numerr2; ?>
    </span> <br>
    <label for="number2">Enter 2nd number :</label>
    <input type="text" name="number2">
    <span style="color:red;">* <?php echo $numerr1; ?>
    <?php echo $numerr2; ?>
    </span> <br>
    <input type="submit" value="Submit" class="button"> <br><br>
    </form>
    <div class="output">

    </div>
    <?php
echo "Sum is : $sum";
?>
</body>
</html>