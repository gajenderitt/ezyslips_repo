<?php

// Get our helper functions
require_once("inc/functions.php");

// Set variables for our request
$shop = "gajender-test";
$token = "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";
$query = array(
	"Content-type" => "application/json" // Tell Shopify that we're expecting a response in JSON format
);

// Run API call to get products
$products = shopify_call($token, $shop, "/admin/products.json", array(), 'GET');

// Convert product JSON information into an array
$products = json_decode($products['response'], TRUE);
echo "<pre>";
print_r($products); die;
// Get the ID of the first product
$product_id = $products['products'][1]['id'];

// Modify product data
$modify_data = array(
	"product" => array(
		"id" => $product_id,
		"title" => "shirt",
		"body_html" => "body_html123"
	)
);

// Run API call to modify the product
$modified_product = shopify_call($token, $shop, "/admin/products/" . $product_id . ".json", $modify_data, 'PUT');

// Storage response
$modified_product_response = $modified_product['response'];
print_r($modified_product_response);