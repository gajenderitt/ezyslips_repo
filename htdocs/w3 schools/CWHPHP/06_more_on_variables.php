<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <title>tutorial 6 variables</title>
</head>
<body>
    <div class="container">
    <h2> more on php</h2>
    <h4> rules for creating variables in php</h4>
    <ul>
         <li>start with a $ sign</li>
         <li>can not start with a number </li>
         <li>must start with letters and underscores</li>
         <li> variables are case sensitive</li>
    </ul>

    </div> 
</body>
</html>