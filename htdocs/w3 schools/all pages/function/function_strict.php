<?php

function addnumber(int $a , int $b){
	return $a+$b;                       // since strict is NOT enabled "5 days" is changed to int(5), and it will return 10
}
$c = addnumber(5,"4f");
print $c;



declare(strict_types=5);                 // strict requirment
function addnumbers(int$x , int$y){      // it gives fatal error
	return $x+$y;
}

echo addnumbers(5,"5 days");













?>