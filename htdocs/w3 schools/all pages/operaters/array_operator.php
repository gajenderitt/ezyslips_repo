<?php

$a = array("hello" => "there", "bye" => "go");
$b = array("c" => "d", "e" => "f");
print_r($a + $b);                                      // union of $a and $b

echo "<br>";

var_dump($a <> $b);                                   // Returns true if $a and $b have the same key/value pairs




//+	    Union	       $x + $y	        Union of $x and $y	
//==	Equality	   $x == $y	        Returns true if $x and $y have the same key/value pairs	
//===	Identity	  $x === $y	Returns true if $x and $y have the same key/value pairs in the same order and of the same types	
//!=	Inequality	    $x != $y	    Returns true if $x is not equal to $y	
//<>	Inequality	    $x <> $y	    Returns true if $x is not equal to $y	
//!==	Non-identity	$x !== $y	    Returns true if $x is not identical to $y









?>