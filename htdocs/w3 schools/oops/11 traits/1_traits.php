<?php

trait table1{
    public function method1(){
        echo "this is method 1";
    }
}

trait table2{
    public function method2(){
        echo "this is method 2";
    }
}

class cat{
    use table1;
}

class dog{
    use table1 , table2;
}

$cats = new cat();
$cats->method1();
echo "<br>";
$dogs = new dog();
$dogs->method2();
$dogs->method1();












?>