<?php

class Fruit {
  public $name;
  public $color;
  public function __construct($name, $color) {
      echo "constructor called <br>";
    $this->name = $name;
    $this->color = $color;
  }
  public function show() {
    echo "The fruit is {$this->name} and the color is {$this->color}."."<br>";
    
  }

  protected function intro() {
    echo "The fruit is protected<br>";
    $this->pri();
    
  }
    private function pri(){
        echo "this is private method<br>";
    }
}

class Mango extends fruit{
    public $name;
    public $color;                    // we must declare property in the child class;
    
    
    public function __construct(){
        parent::__construct($name, $color);   // here we call the constructor of parent class;
        echo "subclass __construct called";
    }
    public function juice(){
        echo "this is mango juice<br>";
        echo "the weight is";
        $this->intro();
       // $apple1 = new Mango("orange","blue");
        parent::intro();
    }
} 
$apple = new Mango("apple","red");
$apple->show();
$apple->juice();




?>