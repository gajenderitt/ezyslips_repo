<?php

class car{
   public $name;
   public $color;

    public function __construct($name,$color){
        echo "construct of parent";
        $this->name = $name;
        $this->color= $color;
    }
        protected function hello(){
            echo "this is the method of parent class name: ".$this->name."<br>";
            echo "the color from parent class is: ".$this->color."<br>";
            echo "weight of car 2 in child class is ".$this->weight."<br>";
        } 
}

class bike extends car{
   
    public $weight;
     
    public function __construct($name,$color,$weight){        // overriding inheritted
        $this->weight = $weight;
        echo "construct of child class"."<br>";
        
    }
        public function mini(){
            echo "this is the child class method"."<br>";
            echo "weight of car in child class is ".$this->weight."<br>";
            $this->hello();
        }
} 
$maruti = new bike("swift","white",100  );

$maruti->mini();



?>