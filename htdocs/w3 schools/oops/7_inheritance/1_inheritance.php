<?php
class fruit{
    public $name;
    public $color;

    public function __construct($name,$color){
        $this->name = $name;
        $this->color = $color;
    }   
        public function bomb(){
            return "name of fruit is {$this->name} and color is {$this->color}";
        }
}

class strawberry extends fruit{
    public function hello(){
        echo "i am strawberry and inheritted by fruit<br>";
    }
}


$apple = new strawberry("apple","red");
$apple->hello();
echo $apple->bomb();




?>
