<?php

interface buddy{
    public function animal_sound();
}

class cat implements buddy{
    public function animal_sound(){
        echo "meow";
    }
}

class dog implements buddy{
    public function animal_sound(){
        echo "bark";
    }
}

class got implements buddy{
    public function animal_sound(){
        echo "mmmmmmmmmmm";
    }
}

$cats = new cat();
$dogs = new dog();
$gots = new got();

$x = array($cats, $dogs, $gots);
foreach($x as $animals){
    $animals->animal_sound();
    echo "<br>";
}

?>