<?php
class car{
    public static function staticmethod(){
        echo "this is static method";
    }
}

car::staticmethod(); echo "<br>";   // call a static method.
echo "-------------------------------------------------------------<br>";

class bike{
    public static function staticmethod1(){
        echo "this is static method 1";
    }

    public function __construct(){
        self::staticmethod1();
    }
}

new bike();          // call static method from method of a class.













?>