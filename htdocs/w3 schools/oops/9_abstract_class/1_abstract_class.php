<?php
abstract class car{
    public $name;

    function __construct($name){
        echo "hello";
        $this->name = $name;
    }

    abstract function intro():string;
}

class audi extends car{

    function intro():string{
        return "i'm from germany, and my name is {$this->name}";
    }
}

class volvo extends car{

    function intro():string{
        return "i'm from canada, and my name is {$this->name}";
    }
}

class bmw extends car{

    function intro():string{
        return "i'm from india, and my name is {$this->name}";
    }
}

$audi1 = new audi("my audi ");
echo $audi1->intro();
echo "<br>";

$volvo1 = new volvo("my volvo ");
echo $volvo1->intro();
echo "<br>";

$bmw1 = new bmw("my bmw ");
echo $bmw1->intro();
echo "<br>";


?>