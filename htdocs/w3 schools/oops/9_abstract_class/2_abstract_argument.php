<?php

abstract class parentclass{
    abstract protected function prefix_name($name);

}

class childclass extends parentclass{
    function prefix_name($name){
        if ($name == "john doe"){
           $prefix = "Mr.";
        }elseif ($name == "jane doe"){
            $prefix = "Mrs.";
        } else{
             $prefix ="";
        }
    return "{$prefix} {$name} ";
    }
}  

$classs = new childclass;
echo $classs->prefix_name("john doe");
echo "<br>";
echo $classs->prefix_name("jane doe");



?>