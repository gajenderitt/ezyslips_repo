<?php
/* 1. Write a PHP program to compute the sum of the two given integer values. If the two values are the same, then returns triple their sum.*/

function atest($x,$y){
	if ($x!=$y){
		echo $x+$y;
	}else{
		echo ($x+$y)*3;
	}                                             // bye me
}

atest(1,2); echo "<br>";
atest(2,3); echo "<br>";
atest(2,2); echo "<br>";


function a_test($x,$y){
	
	return $x==$y ? ($x+$y)*3 : $x+$y;
}
echo a_test(1,2)."\n";                                    // by w3resource
echo a_test(2,3)."\n";  
echo a_test(2,2)."\n";  

/* Write a PHP program to get the absolute difference between n and 51. If n is greater than 51 return triple the absolute difference. */

function btest($x){
	$y = 51;
	if ($x<=$y){
		echo $y-$x;
	}else{
		echo ($x-$y)*3;                    // by me
	}
}
echo "<br>";
btest(53); echo "<br>";
btest(30); echo "<br>";
btest(51);
echo "<br>";


function b_test($n){
	  
	  $x = 51;
	if ($n>$x) {
		return ($n-$x)*3;                // by w3resource
	}
	return ($x-$n);
}


echo b_test(53); echo "<br>";
echo b_test(30); echo "<br>";
echo b_test(51);










?>