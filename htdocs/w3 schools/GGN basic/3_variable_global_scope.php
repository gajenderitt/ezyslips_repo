<?php

// this is global scope
$x = 5;

function mytest(){
  echo "<p>variable x inside the function is: $x</p>";
}
mytest();

echo "variable x outside the function is: $x";


// this is local scope




function test(){
	$y = 5;
  echo "<p>variable y inside the function is: $y</p>";
}
test();

echo "variable y outside the function is: $y";














?>