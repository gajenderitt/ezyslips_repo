<?php
	
class car{
	public $color;
	public $model;
	public function __construct($color,$model){
		$this->color = $color;
		$this->model = $model;
	}
	public function message(){
		return "my car is a ". $this->color ." ". $this->model. "!";
	}
}

$mycar = new car("white","rolls royce");
echo $mycar -> message();
echo "<br>";
$mycar = new car("red", "ferrari");
echo $mycar -> message();

// null data type

$y = "hello";
$y = null;
var_dump($y);













?>