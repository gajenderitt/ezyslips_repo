<?php

/*Write a PHP program to check a given integer and return true if it is within 10 of 100 or 200. */

function test($x){
    return (abs(100-$x) <= 10)  || (abs(200-$x) <= 10);
}


var_dump(test(30));
echo "<br>";
var_dump(test(91));
echo "<br>";
var_dump(test(90));
echo "<br>";
var_dump(test(191));
echo "<br>";
var_dump(test(110));
echo "<br>";
var_dump();













?>