<?php
/* 5. Write a PHP program to create a new string where 'if' is added to the front of a given string.
 If the string already begins with 'if', return the string unchanged.  */

function m_test($x){
    if (strlen($x) > 2 && substr($x,0,2) == "if"){
        return $x;
    }
    return "if ".$x;
}  

echo m_test("if else");
echo "<br>";
echo m_test("else");
echo "<br>";



?>