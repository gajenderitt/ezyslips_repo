<?php
/*
Write a PHP program to exchange the first and last characters in a given string and return the new string. 
Sample Input:
"abcd"
"a"
"xy"
Sample output:
dbca , a  ,  yx


$x = "hello";
$z = strlen($x)-1;         // for testing
echo $z;
*/



function test($x){
    $z = strlen($x);
    $a = strlen($x)-1;
    for($y=0;$y<$z;$y++){
        if($y==0){
            echo $x[$a];
            continue;
        } 
        if($y==$a){
            echo $x[0];
            continue;
        }
    echo $x[$y];
    

    }
}

test("abcd");
echo "<br>";
test("x");
echo"<br>";
test("xy");
echo "<br>";
test("gajender");


/*
function test($str) 
{
   return strlen($str) > 1 ? substr($str, strlen($str) - 1).substr($str, 1, strlen($str) - 2). substr($str, 0, 1) : $str;
   
}

echo test("abcd")."\n";
echo test("a")."\n";
echo test("xy")."\n";
*/

?>