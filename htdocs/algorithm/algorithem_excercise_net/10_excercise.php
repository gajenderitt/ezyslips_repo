<?php
/*Write a PHP program to check if a given positive number is a multiple of 3 or a multiple of 7. 
Sample Input
3
14
12
37
Sample Output:
bool(true) , bool(true) , bool(true) , bool(false)
*/


function test($z){
       var_dump($z%3==0 || $z%7==0);

}
test(3);
echo "<br>";
test(14);
echo "<br>";
test(12);
echo "<br>";
test(37);
echo "<br>";


/*
function test($n) 
{
   return $n % 3 == 0 || $n % 7 == 0;
}

var_dump(test(3));
var_dump(test(14));
var_dump(test(12));
var_dump(test(37));
*/

?>