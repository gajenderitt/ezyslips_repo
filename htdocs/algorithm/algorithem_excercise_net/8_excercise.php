<?php
/*
8. Write a PHP program to create a new string which is 4 copies of the 2 front characters of a given string.
 If the given string length is less than 2 return the original string. Go to the editor
Sample Input:
"C Sharp"
"JS"
"a"
Sample Output:
C C C C  ,  JSJSJSJS  ,  a
*/


function test($z){
    $x = strlen($z);
    if($x>=2){
        for($a=0;$a<4;$a++){
            echo $z[0],$z[1];
        }

    }else{
        echo $z;
    }
}

test("C SHARP");
echo"<br>";
test("JS");
echo"<br>";
test("A");
echo"<br>";



/*
<?php
function test($str) 
{
   return strlen($str) < 2 ? $str : substr($str, 0, 2).substr($str, 0, 2).substr($str, 0, 2).substr($str, 0, 2);
   
}

echo test("C Sharp")."\n";
echo test("JS")."\n";
echo test("a")."\n";
*/

?>