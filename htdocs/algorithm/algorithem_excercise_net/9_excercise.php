<?php
/*. Write a PHP program to create a new string with the last char added
 at the front and back of a given string of length 1 or more. 
Sample Input:
"Red"
"Green"
"1"
Sample Output:
dRedd  ,  nGreenn  ,  111
*/

function test($z){
    $a = strlen($z);
    $b = strlen($z)-1;
    if ($a>=1){
        for($x=0;$x<$a;$x++){
            if($x==0){
                echo $z[$b];
                //echo $z;          we can use this  also for print string.
            }if($x==$b){
                echo $z[$b];
            }
            echo $z[$x];
        }
    }
}

test("Red");
echo "<br>";
test("Green");
echo "<br>";
test("1");
echo "<br>";



/*                          by w3resource
function test($str) 
{
   $s = substr($str, strlen($str) - 1);
      return $s.$str.$s;
   
}

echo test("Red")."\n";
echo test("Green")."\n";
echo test("1")."\n";
*/






















?>